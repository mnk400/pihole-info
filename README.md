## Pi-Hole Status

Simple script written uses curses to print information from your pi-hole on your terminal without needing to SSH into your pi or havin to open the web interface. Script refreshes once every 30 seconds

![](img/terminal-screenshot.png)
```sh

$ git clone https://github.com/mnk400/pihole-info
$ cd pihole-info
$ python3 piholeinfo.py
```


